/*

splitOnDoubleLetter(‘Letter’) -> [‘let’, ‘ter’]
splitOnDoubleLetter(‘Really’) -> [‘real’, ‘ly’]
splitOnDoubleLetter(‘Happy’) -> [‘hap’, ‘py’]
splitOnDoubleLetter(‘Shall’) -> [‘shal’, ‘l’]
splitOnDoubleLetter(‘Tool’) -> [‘to’, ‘ol’]
splitOnDoubleLetter(‘Mississippi’) -> [‘Mis’, ‘sis’, ‘sip’, ‘pi’]
splitOnDoubleLetter(‘Easy) returns []

*/

function splitOnDoubleLetter(string) {
    const stringArray = []
    let lastMatchIndex = 0

    for (let currentIndex = 0; currentIndex < string.length; currentIndex++) {
        const character = string[currentIndex]
        
        if (currentIndex <= 0 || character.toLowerCase() !== string[currentIndex - 1].toLowerCase())
            continue

        stringArray.push(string.substring(lastMatchIndex, currentIndex))

        lastMatchIndex = currentIndex
    }

    stringArray.push(string.substring(lastMatchIndex))

    return stringArray
}

console.log("splitOnDoubleLetter('Letter') -> ", splitOnDoubleLetter("Letter"));
console.log("splitOnDoubleLetter('Really') -> ", splitOnDoubleLetter("Really"));
console.log("splitOnDoubleLetter('Happy') -> ",  splitOnDoubleLetter("Happy"));
console.log("splitOnDoubleLetter('Letter') -> ", splitOnDoubleLetter("Shall"));
console.log("splitOnDoubleLetter('Letter') -> ", splitOnDoubleLetter("Mississippi"));
console.log("splitOnDoubleLetter('Letter') -> ", splitOnDoubleLetter("Easy"));